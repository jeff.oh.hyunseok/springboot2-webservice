# springboot2-webservice

# 프로젝트 셋팅
### 그래들 버전 다운그레이드
- 터미널을 열고
> gradlew wrapper --gradle-version 4.10.2


# 폴더 구조

- web
### domain
> 도메인을 담을 패키지
- 도메인이란?
- 게시글, 댓글, 회원, 정산, 결제 등 소프트웨어에 대한 요구사항 혹은 문제 영역

### API
- Dto : Request 데이터를 받음
- Controller : API 요청을 받음
- Service : 트랜잭션, 도메인 기능 간의 순서를 보장함