package com.jeff.springboot.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Getter // Getter 메소드 자동 생성
@NoArgsConstructor // 롬복 기본 생성자 자동 추가 public Posts() {}
@Entity // 테이블과 링크될 클래스 ex Post.Java -> posts table, Entity 클래스 에서는 절대 Setter를 만들지 않는다 대신 메소드를 추가한다.
public class Posts extends BaseTimeEntity {

    @Id // Primary Key
    @GeneratedValue(strategy = GenerationType.IDENTITY) // PK 생성 규칙
    private Long id;

    @Column(length = 500, nullable = false) // 테이블의 컬럼을 나타냄
    private String title;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;
    private String author;

    @Builder // 빌더 패턴 클래스 생성, 생성자 상단에 선언 시 생성자에 포함된 필드만 빌더에 포함됨
    public Posts(String title, String content, String author) {
        this.title = title;
        this.content = content;
        this.author = author;
    }

    public void update(String title, String content) {
        this.title = title;
        this.content = content;
    }
}
