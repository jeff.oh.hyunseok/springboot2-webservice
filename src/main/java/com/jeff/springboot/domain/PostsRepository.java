package com.jeff.springboot.domain;

import org.springframework.data.jpa.repository.JpaRepository;

// CRUD가 자동으로 생성된다.
// Entity 클래스와 기본 Entity클래스는 함께 위치해야 한다.
// Entity 클래스는 기본 Repository 없이는 제대로 역할을 할 수 없다.
public interface PostsRepository extends JpaRepository<Posts,Long> {}
