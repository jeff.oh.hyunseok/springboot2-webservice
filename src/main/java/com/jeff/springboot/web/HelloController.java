package com.jeff.springboot.web;

import com.jeff.springboot.web.dto.HelloResponseDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController // 컨트롤러를 JSON으로 반환하는 컨트롤러로 만듬
public class HelloController {
    @GetMapping("/hello") // HTTP Get 요청을 받을 수 있도록 API 만듬
    public String hello() {
        return "hello";
    }

    @GetMapping("/hello/dto")
    public HelloResponseDto helloDto(@RequestParam("name") String name, @RequestParam("amount") int amount)
    {
        // RequestParam : 외부 API로 넘긴 파라미터를 가져옴
        return new HelloResponseDto(name, amount);
    }
}
