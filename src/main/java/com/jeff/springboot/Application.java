package com.jeff.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing // JPA Auditing 활성화
@SpringBootApplication
public class Application { // 프로젝트 메인 클래스, 프로젝트 최 상단에 위치해야 함
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
